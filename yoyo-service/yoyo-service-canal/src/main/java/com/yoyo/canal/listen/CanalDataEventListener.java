package com.yoyo.canal.listen;

/**
 * @Author Chaiyym
 * @Date 2020/9/26 2:42
 * @Version 1.0
 */
//@CanalEventListener
public class CanalDataEventListener {

/*
    *//****
     * 增加数据变化获取
     * CanalEntry.EventType:记录当前监听的事件  增加
     * CanalEntry.RowData:变化的那一行记录
     * rowData.getAfterColumnsList():发生变化后列的值集合
     *//*
    //@InsertListenPoint
    public void onEventInsert(CanalEntry.EventType eventType,CanalEntry.RowData rowData){
        for (CanalEntry.Column column : rowData.getAfterColumnsList()) {
            System.out.println(column.getName()+":"+column.getValue());
        }
    }


    *//***
     * 修改数据变化获取
     * rowData.getBeforeColumnsList():发生变化前列的值集合
     *//*
    //@UpdateListenPoint
    public void onEventUpdate(CanalEntry.EventType eventType,CanalEntry.RowData rowData){
        //修改前
        System.out.println("============发生变化前记录=========");
        for (CanalEntry.Column column : rowData.getBeforeColumnsList()) {
            System.out.println(column.getName()+":"+column.getValue());
        }

        // 修改后
        System.out.println("============发生变化后前记录=========");
        for (CanalEntry.Column column : rowData.getAfterColumnsList()) {
            System.out.println(column.getName()+":"+column.getValue());
        }
    }


    *//***
     * 删除数据变化获取
     *//*
    //@DeleteListenPoint
    public void onEventDelete(CanalEntry.EventType eventType,CanalEntry.RowData rowData){
        //修改前
        System.out.println("============发生变化前记录=========");
        for (CanalEntry.Column column : rowData.getBeforeColumnsList()) {
            System.out.println(column.getName()+":"+column.getValue());
        }
    }

    *//***
     * 获取指定列的值
     *//*
    public static String getCloumValue(String name,CanalEntry.RowData rowData){
        for (CanalEntry.Column column : rowData.getAfterColumnsList()) {
            //匹配列的名字是否一致
            if(name.equals(column.getName())){
                return column.getValue();
            }
        }
        return null;
    }*/
}
