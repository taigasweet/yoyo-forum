package com.yoyo.user.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yoyo.pojo.user.User;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author quanmeng
 * @since 2020-09-26
 */
public interface UserMapper extends BaseMapper<User> {

}
