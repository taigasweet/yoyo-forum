package com.yoyo.user;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import springfox.documentation.oas.annotations.EnableOpenApi;

/**
 * @Author Chaiyym
 * @Date 2020/9/26 0:27
 * @Version 1.0
 */
@EnableOpenApi
@SpringBootApplication
@MapperScan(basePackages = {"com.yoyo.user.dao"})
@EnableEurekaClient
public class UserApplication {
    public static void main(String[] args) {
        SpringApplication.run(UserApplication.class, args);
    }
}

