package com.yoyo.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yoyo.pojo.user.User;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author quanmeng
 * @since 2020-09-26
 */
public interface IUserService extends IService<User> {

}
