package com.yoyo.user.api;

import com.yoyo.pojo.user.User;
import com.yoyo.response.vo.ResponseData;
import com.yoyo.response.vo.ResponsePage;
import com.yoyo.response.vo.ResponseResult;
import com.yoyo.vo.QueryUserPageVO;
import io.swagger.annotations.*;

import javax.servlet.http.HttpServletResponse;

/**
 * @Author Chaiyym
 * @Date 2020/9/26 0:37
 * @Version 1.0
 */
@Api(value = "UserControllerApi", tags = "用户管理api")
public interface UserControllerApi {

    /**
     *
     * @param userName 用户id
     * @param password 用户登录密码
     * @return token
     */
    @ApiOperation("登录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userName", value = "用户登录名", required = true, dataType = "string"),
            @ApiImplicitParam(name = "password", value = "用户登录密码", required = true, dataType = "string")
    })
    ResponseData<String> login(String userName, String password, HttpServletResponse response);

    /**
     * 新增用户
     * @param user 用户信息
     * @return 是否成功
     */
    @ApiOperation("新增用户")
    ResponseResult addUser(@ApiParam("用户信息") User user);

    /**
     * 修改用户
     * @param user 用户信息
     * @return 是否成功
     */
    @ApiOperation("修改用户")
    ResponseResult updateUser(@ApiParam("用户信息") User user);

    /**
     * 删除用户
     * @param userId 用户id
     * @return 是否成功
     */
    @ApiOperation("删除用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", required = true, dataType = "string")
    })
    ResponseResult delUserById(Long userId);

    /**
     * 查询用户
     * @param userId 用户id
     * @return 用户信息
     */
    @ApiOperation("查询用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", required = true, dataType = "string")
    })
    ResponseData<User> selUserById(Long userId);

    /**
     * 用户分页列表
     * @param queryUserPageVo 筛选条件
     * @return 分页列表
     */
    @ApiOperation("用户分页列表")
    ResponsePage<User> queryUserPage(@ApiParam("筛选条件") QueryUserPageVO queryUserPageVo);
}
