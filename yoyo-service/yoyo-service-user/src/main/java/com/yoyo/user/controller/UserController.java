package com.yoyo.user.controller;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yoyo.base.BaseController;
import com.yoyo.pojo.user.User;
import com.yoyo.response.CommonCode;
import com.yoyo.response.vo.ResponseData;
import com.yoyo.response.vo.ResponsePage;
import com.yoyo.response.vo.ResponseResult;
import com.yoyo.user.api.UserControllerApi;
import com.yoyo.user.service.IUserService;
import com.yoyo.util.JwtUtil;
import com.yoyo.vo.QueryUserPageVO;
import org.apache.commons.lang.StringUtils;

import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Cookie;
import java.util.*;

/**
 * @Author Chaiyym
 * @Date 2020/9/26 0:51
 * @Version 1.0
 */
@RestController
@RequestMapping("user")
public class UserController extends BaseController implements UserControllerApi {
    @Resource
    private IUserService userService;

    @Override
    @PostMapping("login")
    public ResponseData<String> login(String userName, String password, HttpServletResponse response) {
        Optional<User> user = userService.lambdaQuery()
                .select(User::getUserId)
                .eq(User::getAccessName, userName)
                .eq(User::getAccessPassword, password)
                .oneOpt();
        if (user.isPresent() && password.equals(user.get().getAccessPassword())) {
            Map<String, Object> info = new HashMap<>(3);
            info.put("role", "USER");
            info.put("success", "SUCCESS");
            info.put("username", userName);
            String jwt = JwtUtil.createJWT(UUID.randomUUID().toString(), JSON.toJSONString(info), null);
            Cookie cookie = new Cookie("Authorization", jwt);
            response.addCookie(cookie);
            //生成令牌
            if (StringUtils.isNotBlank(jwt)) {
                return new ResponseData<>(CommonCode.SUCCESS, jwt);
            }
        }
        return new ResponseData<>(CommonCode.UNAUTHENTICATED);
    }

    @Override
    @PostMapping
    public ResponseResult addUser(@RequestBody User user) {
        boolean save = userService.save(user);
        if (!save) {
            return ResponseResult.FAIL();
        }
        return ResponseResult.SUCCESS();
    }

    @Override
    @PutMapping
    public ResponseResult updateUser(@RequestBody User user) {
        userService.saveOrUpdate(user);
        return ResponseResult.SUCCESS();
    }

    @Override
    @DeleteMapping("{userId}")
    public ResponseResult delUserById(@PathVariable("userId") Long userId) {
        boolean b = userService.removeById(userId);
        if (!b) {
            return ResponseResult.FAIL();
        }
        return ResponseResult.SUCCESS();
    }

    @Override
    @GetMapping("{userId}")
    public ResponseData<User> selUserById(@PathVariable("userId") Long userId) {
        User byId = userService.getById(userId);
        return new ResponseData<>(CommonCode.SUCCESS, byId);
    }

    @Override
    @GetMapping("page")
    public ResponsePage<User> queryUserPage(QueryUserPageVO queryUserPageVo) {
        queryUserPageVo.initializeParams();
        Page<User> page = userService.lambdaQuery()
                .select(User::getUserId
                        , User::getUserName
                        , User::getUserAge
                        , User::getCreateTime)
                .eq(Objects.nonNull(queryUserPageVo.getAge()), User::getUserAge, queryUserPageVo.getAge())
                .like(StringUtils.isNotBlank(queryUserPageVo.getSearchContent()), User::getUserName, queryUserPageVo.getSearchContent())
                .page(new Page<>(queryUserPageVo.get_page(), queryUserPageVo.get_limit()));
        return new ResponsePage<>(page.getRecords(), page.getTotal());
    }
}
