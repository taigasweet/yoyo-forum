package com.yoyo.response.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * kv返回
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FrontSpinner {
	@ApiModelProperty("键")
	private Integer snVal;
	
	@ApiModelProperty("值")
	private String snName;
}
