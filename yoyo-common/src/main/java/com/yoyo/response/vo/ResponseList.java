package com.yoyo.response.vo;

import com.yoyo.response.ResultCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * list返回
 * @param <T>
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ResponseList<T> extends ResponseResult {
    List<T> list = new ArrayList<>();


    public ResponseList(ResultCode resultCode) {
        super(resultCode);
    }
}
