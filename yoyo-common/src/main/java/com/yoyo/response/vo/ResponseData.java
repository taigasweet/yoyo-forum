package com.yoyo.response.vo;

import com.yoyo.response.ResultCode;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 单对象返回
 * @param <T>
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ResponseData<T> extends ResponseResult {
    T data = null;

    public ResponseData(ResultCode resultCode, T data) {
        super(resultCode);
        this.data = data;
    }

    public ResponseData(ResultCode resultCode) {
        super(resultCode);
    }
}
