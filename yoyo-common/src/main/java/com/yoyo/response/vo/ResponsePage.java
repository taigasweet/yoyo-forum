package com.yoyo.response.vo;

import com.yoyo.response.ResultCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 分页返回
 * @param <T>
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ResponsePage<T> extends ResponseList<T> {
	long total = 0;

	public ResponsePage(List<T> collect, long total) {
		super(collect);
		this.total=total;
	}

	public ResponsePage(ResultCode resultCode) {
		super(resultCode);
	}

}
