package com.yoyo.util;

import java.io.Serializable;

/**
 * @Author Chaiyym
 * @Date 2020/9/25 23:36
 * @Version 1.0
 */
public class StatusCode implements Serializable {
    //成功
    public static final int OK = 20000;

    //失败
    public static final int ERROR = 20001;

    //用户名或密码错误
    public static final int LOGIN_ERROR = 20002;

    //权限不足
    public static final int ACCESS_ERROR = 20003;

    //远程调用失败
    public static final int REMOT_EERROR = 20004;

    //重复操作
    public static final int REP_ERROR = 20005;
}
