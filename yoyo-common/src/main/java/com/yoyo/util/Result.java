package com.yoyo.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author Chaiyym
 * @Date 2020/9/25 23:42
 * @Version 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Result<T> {

    //是否成功
    private boolean flag;

    //返回码
    private Integer code;

    //返回消息
    private String message;

    //返回数据
    private T data;


}
