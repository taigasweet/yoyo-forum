package com.yoyo.base;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

/**
 * @Author Chaiyym
 * @Date 2020/9/26 1:06
 * @Version 1.0
 */
@Configuration
public class BaseController {

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        // 解决下划线开头参数无法映射到实体的问题
        binder.setFieldMarkerPrefix(null);
    }
}
