package com.yoyo.base;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Optional;

/**
 * @Author Chaiyym
 * @Date 2020/9/26 0:43
 * @Version 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "查询条件通用对象")
public class BasePageQuery {

    @ApiModelProperty("检索内容")
    String  searchContent;

    @ApiModelProperty("页码")
    Integer _page;

    @ApiModelProperty("条数")
    Integer _limit;

    public void initializeParams() {
        _page = Optional.ofNullable(_page).orElse(1);
        _limit = Optional.ofNullable(_limit).orElse(10);
        searchContent = Optional.ofNullable(searchContent).orElse("").trim();
        searchContent = searchContent.isEmpty() ? null : searchContent;
    }

}
