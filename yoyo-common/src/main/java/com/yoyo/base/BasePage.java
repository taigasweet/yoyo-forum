package com.yoyo.base;

import lombok.*;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode()
@Accessors(chain = true)
@Builder
public class BasePage<T> {
    //数据
    private List<T> data;

    //count
    Integer total;

    public static BasePage BasicRes() {
        return BasePage.builder()
                .data(new ArrayList<>())
                .total(0).build();
    }
}
