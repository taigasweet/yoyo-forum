# YOYO论坛

#### 介绍
当前还在做的过程，参考若依

#### 软件架构
软件架构说明

1.yoyo-forum:

    父工程管理所有依赖

2.yoyo-common：

    通用工具工程、主要以Hutool工具类为主

3.yoyo-db：
    
    数据库链接工程
    针对mysql和mongodb做出整改

4.yoyo-eureka:
    
    微服务注册中心 7001

5.yoyo-gateway:
    
    微服务网关 8001

6.yoyo-service:

    微服务管理工程（微服务父工程）

7.yoyo-service-api:

    微服务组件父工程（给各个微服务提供支持）

8.yoyo-user-oauth:

    认证中心

9.yoyo-web:

    页面服务父工程

####中间件
1.mysql:

    ip/port:
    des:

2.zookeeper:

    ip/port:
    des:
    
3.rabbitmq:

    ip/port:
    des:

4.redis

    ip/port:
    des:
    
5.elasticSearch

    ip/port:
    des:
    
6.logstash

    ip/port:
    des:
7.kibana

    ip/port:
    des:

8.filebeat

    ip/port:
    des:

9.kafka

    ip/port:
    des:





