package com.yoyo.vo;

import com.yoyo.base.BasePageQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @Author Chaiyym
 * @Date 2020/9/26 0:42
 * @Version 1.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "用户分页查询对象")
public class QueryUserPageVO extends BasePageQuery {
    @ApiModelProperty(value = "用户年龄")
    private Integer age;
}
